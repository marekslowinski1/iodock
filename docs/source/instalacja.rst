Instalacja
===========================
Do uruchomienia aplikacji w kontenerze konieczne jest poprawne zainstalowanie
Dockera wraz z Docker Compose oraz dostęp do komend docker i docker-compose z terminala.
Aby zainstalować Dockera podążaj za `oficjalną dokumentacją <https://docs.docker.com/get-docker/>`_.

Aby uruchomić aplikację przejdź do katalogu głównego zawierającego plik
docker-compose.yaml.
Wywołaj komendę::

    $docker-compose up;

Pierwsze uruchomienie potrwa dłużej od kolejnych, gdyż Docker
pobierze kontenery bazowe oraz zbuduje kontener zdefiniowany
w pliku Dockerfile. Kolejne uruchomienia będą trwały krócej.

Aplikacja webowa zostanie domyślnie uruchomiona na porcie 8080.