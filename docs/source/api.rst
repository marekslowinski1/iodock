Api
===========================
GET /customers
-------------------------
Zwraca listę wszystkich klientów.

GET /customers/{customerId}
----------------------------
Zwraca dane klienta o podanym *customerId* jeśli dany klient istnieje
lub kod **404 Not Found** jeśli w bazie nie ma klienta o podanym id. 
