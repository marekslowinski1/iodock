.. iodock documentation master file, created by
   sphinx-quickstart on Sat May 22 17:47:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Dokumentacja iodock!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Spis treści:

   wprowadzenie
   instalacja
   architektura
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
