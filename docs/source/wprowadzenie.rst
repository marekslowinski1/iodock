Wprowadzenie
===========================
docker
-------------------------
Docker to narzędzie do tworzenia wirtualnego środowiska programistycznego
w postaci tzw. kontenera. Kontener zawiera wszystkie niezbędne narzędzia
do tworzenia lub uruchamiania oprogramowania. Dzięki temu deweloper zyskuje
dostęp do skonfigurowanego środowiska i nie musi poświęcać czasu na konfigurację
własnego systemu i instalację niezbędnych zależności. 

Budowanie i uruchamianie oprogramowania w kontenerach umożliwia uniezależnienie od
systemu operacyjnego i ułatwia automatyczne budowanie, testowanie i wdrażanie aplikacji
przy wykorzystaniu systemów CI (np. Github Actions, Travis, Gitlab Ci/CD). 

Zazwyczaj wykorzystuje się kontener bazowy będący dystrybucją systemu Linux 
a zawartość kontenera jest zdefiniowana w pliku konfiguracyjnym w formie kroków 
niezbędnych do skonfigurowania środowiska: instalacja zależności, ustawienie zmiennych systemowych, 
stworzenie użytkowników z odpowiednimi uprawnieniami. 

docker-compose
-------------------------
Docker Compose pozwala zarządzać uruchamianiem kontenerów w przypadku, gdy
do poprawnego działania oprogramowania wymaganych jest wiele usług, a każdą z nich
postanowiono uruchamiać w oddzielnym kontenerze.
W tym celu stosuje się plik konfiguracyjny, w którym definiuje się 
scenariusz uruchamiania aplikacji i zależności pomiędzy kontenerami.