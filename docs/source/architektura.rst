Architektura
===========================
Diagram klas
-------------------------

.. uml::

    title Struktura projektu


    class Customer {
      .. Constructor ..
      +Customer(String firstName, String lastName)

      .. Fields ..
      -id: Long
      -firstName: String
      -lastName: String

      .. Methods ..
      +Long getId()
      +String getFirstName()
      +String getLastName()
      +String toString()
    }

    interface CustomerRepository {
      +List<Customer> findByLastName(String lastName)
      +Customer findById(long id);   
    }
    
    class CustomerController {
      +Customer getUserById(Long customerId)
      +Iterable<Customer> getCustomers()
    }

    CustomerController <|-down- CustomerRepository
    CustomerRepository <|-down- Customer


Diagram przypadków użycia
--------------------------

.. uml::

    title Customer Manager

    actor :Admin: as A
    
    A -> (Find specific customer)
    A -up-> (Find all customers)